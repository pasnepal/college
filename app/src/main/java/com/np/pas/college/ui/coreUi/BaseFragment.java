package com.np.pas.college.ui.coreUi;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.np.pas.college.ui.vInterface.IView;
import com.np.pas.college.utils.ProgressDialogFactory;
import butterknife.Unbinder;


public abstract class BaseFragment<L extends IView> extends Fragment implements IView {
    public Context context;
    public L listener;
    public Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createPresenter();
        setContext();
    }

    public abstract void setContext();


    @Override
    public void showToast(String message) {
        if (context != null)
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public void showLoading() {
        ProgressDialogFactory.getInstance(getActivity(), "Loading").show();
    }

    @Override
    public void hideLoading() {
        ProgressDialogFactory.dismiss();
    }

}
