package com.np.pas.college.ui.vInterface;

import android.content.Context;

/**
 * Created by Rashmi on 11/26/2017.
 */

public interface  IView {
    void showToast(String message);

    void showLoading();

    void hideLoading();

    void createPresenter();

    Context getContext();
}
