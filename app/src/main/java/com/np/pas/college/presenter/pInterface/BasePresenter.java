package com.np.pas.college.presenter.pInterface;


import com.np.pas.college.ui.vInterface.IView;

/**
 * Created by Rashmi on 11/26/2017.
 */

public interface BasePresenter {
    IView getView();
}
