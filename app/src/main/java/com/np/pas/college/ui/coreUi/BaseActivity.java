package com.np.pas.college.ui.coreUi;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.np.pas.college.presenter.pInterface.BasePresenter;
import com.np.pas.college.ui.vInterface.IView;
import com.np.pas.college.utils.ProgressDialogFactory;


/**
 * Created by pranishshrestha on 5/1/17.
 */

public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity implements IView {

    public Context context;
    public P presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createPresenter();
        setContext();
    }

    /**
     * Call from onCreate method from all child classes
     */
    public abstract void setContext();

    @Override
    public void showToast(String message) {
        if (context != null)
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public void showLoading() {
        ProgressDialogFactory.getInstance(context, "Loading").show();
    }

    @Override
    public void hideLoading() {
        ProgressDialogFactory.dismiss();
    }
}
