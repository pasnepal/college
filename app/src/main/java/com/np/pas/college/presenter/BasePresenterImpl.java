package com.np.pas.college.presenter;

import com.np.pas.college.presenter.pInterface.BasePresenter;
import com.np.pas.college.ui.vInterface.IView;

/**
 * Created by Rashmi on 11/26/2017.
 */

public class BasePresenterImpl<V extends IView> implements BasePresenter {
    private V iView;

    public BasePresenterImpl(V iView) {
        this.iView = iView;
    }

    @Override

    public V getView() {
        return iView;
    }
}
