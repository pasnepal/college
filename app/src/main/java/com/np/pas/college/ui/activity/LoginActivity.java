package com.np.pas.college.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.np.pas.college.R;

import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }
}
