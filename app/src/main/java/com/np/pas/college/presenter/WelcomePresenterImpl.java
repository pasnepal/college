package com.np.pas.college.presenter;

import com.np.pas.college.presenter.pInterface.WelcomePresenter;
import com.np.pas.college.ui.vInterface.IView;

/**
 * Created by Rashmi on 11/27/2017.
 */

public class WelcomePresenterImpl extends BasePresenterImpl implements WelcomePresenter {
    public WelcomePresenterImpl(IView iView) {
        super(iView);
    }
}
